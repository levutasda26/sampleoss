1. Fork this project
1. Clone your fork
1. Open an issue on this project describing the issue you are addressing
1. Create a branch on your project
1. Make your commits to your branch
   * Commit messages must follow conventional commit format
   * Acceptable commit types are: `build`, `chore`, `ci`, `docs`, `feat`, `fix`, `perf`, `refactor`, `revert`, `style`, or `test`
   * Commit subject lines must be <= 72 characters
   * Commit subject line must include issue #
1. Push your branch to your fork
1. Open a merge request to merge your branch into this project
   * Reference the issue in the merge request
1. Await review
1. Make any necessary changes
